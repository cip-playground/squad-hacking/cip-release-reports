FROM python:3.9-slim

RUN apt-get update
RUN apt-get install -y --no-install-recommends tree
RUN apt-get install -y weasyprint
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

WORKDIR /app
COPY . .

RUN pip install --upgrade pip
RUN pip install .
