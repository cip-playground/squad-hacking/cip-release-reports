# CIP release reports

Generates release reports for all CIP Linux Kernel builds.

## Overview

The release report overview can be accessed here:
<https://cip-playground.gitlab.io/squad-hacking/cip-release-reports>.

## Contents

By default, the CI/CD pipeline in this GitLab project generates release reports
for each build in each SQUAD project within the SQUAD group
[`linux-cip`](https://squad.ciplatform.org/linux-cip/). 

Each report starts of whether the build contains failed tests (excluding known
issues) or not. If that is the case, an expandable section with failed tests
comes next. After that follow expandable sections with known issues, passed tests, skipped tests and incomplete test suites.

At the bottom, a section with build results is shown for each kernel
configuration. Please note that not all builds are tested.

### Test/build result duplicates

The algorithm, that generates the reports, identifies multiple results for the
same test or build and only reports the newest result (highest test `id`). Older
results will be discarded.

### Incomplete jobs

The reports feature a section with incomplete tests. This section actually
doesn't contain any test results, but test suites for given devices that never
ran due to a problem with the corresponding LAVA test job. 

### Update mechanism

By storing a so-called register (`register.yaml`) along with all the generated reports in a dedicated branch called `pages` as well as the GitLab pages site, rebuild existing release reports can be avoided.

Prior to building, the algorithm pulls the previous build results and register
from the branch `pages`, consolidates, generates only reports of new builds and
updates the register. Then the updated results and register are pushed to the
branch `pages` and finally also to GitLab Pages.  
It is also possible to force to rebuild reports, in case a build has changed and
the old report isn't valid anymore.

## Usage

The tool to generates CIP release reports is provided as a python package, which
provides the command `cip-release-reports`. This package is provisioned inside a
docker container, that can be found in the registry of this GitLab project. This
allows usage in other GitLab projects.

By issueing the the command `cip-release-reports --help` all possible options of
the tools are shown:

```
usage: cip-release-reports [-h] [--base_dir BASE_DIR] [--squad_url SQUAD_URL] [--group GROUP] [--project PROJECT] [--build BUILD] [--force]

Build release reports for the CIP project

options:
  -h, --help            show this help message and exit
  --base_dir BASE_DIR   Base directory for reports
  --squad_url SQUAD_URL
                        URL for SQUAD API
  --group GROUP         Group slug
  --project PROJECT     Project slug
  --build BUILD         Build slug
  --force               Force rebuild of reports
```

### CI/CD Pipeline

Reports are updated by rerunning the [CI/CD
pipeline](https://gitlab.com/cip-playground/squad-hacking/cip-release-reports/-/pipelines/new). The pipeline also runs on a daily basis.

## License

An Apache style license applies to this project. Please refer to the
[LICENSE](LICENSE) file for more details.
