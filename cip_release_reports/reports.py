""" Tools to generate reports from SQUAD"""

import pathlib as pl
from squad_client.core.api import SquadApi
from squad_client.core.models import Squad
from cip_release_reports import tools
from cip_release_reports import squad_tools
from cip_release_reports import fetcher
from cip_release_reports import register


def build_reports(squad_url='https://squad.ciplatform.org',
                  output_dir='.',
                  group_slug='linux-cip',
                  project_slug='linux-6.1.y-cip',
                  **kwargs):
    """ Generate build report """
    group = Squad().group(group_slug)
    project = group.project(project_slug)
    reporter = fetcher.ProjectReporter(squad_url, project)
    build_slugs = kwargs.pop('builds', squad_tools.project_build_versions(project))
    for build_slug in build_slugs:
        print('    build:', build_slug, end=' ')
        build_present = kwargs['register'].was_built(group_slug, project_slug, build_slug)
        if build_present:
            if not kwargs.get('force', False):
                print('(skipping, already built)')
                yield group_slug, project_slug, build_slug
                continue
            print('(rebuilding)')
        else:
            print('(building)')
        build = squad_tools.acquire_build(project, build_slug)
        output_file = f'{build_slug}.html'
        reporter.generate(project_slug, build, output_dir, output_file)
        yield group_slug, project_slug, build_slug


def project_reports(base_dir, squad_url, group, **kwargs):
    """ Generate reports for all projects in a group """
    project_slugs = kwargs.pop('projects', squad_tools.group_project_slugs(group))
    for project in project_slugs:
        print('  project:', project)
        output_dir = base_dir / group / project
        output_dir.mkdir(parents=True, exist_ok=True)
        yield from build_reports(
            squad_url=squad_url,
            output_dir=output_dir,
            group_slug=group,
            project_slug=project,
            **kwargs,
        )


def group_reports(base_dir, squad_url, **kwargs):
    """ Generate index report for all projects in a group """
    group_slugs = kwargs.pop('groups', squad_tools.group_slugs())
    for group in group_slugs:
        print('group:', group)
        yield from project_reports(base_dir, squad_url, group, **kwargs)


def gen_overview(build_register):
    """ Generate overview report for all projects in a group """
    build_ids = dict(squad_tools.all_build_id_verions())
    yield '# CIP Release Reports\n'
    for group_slug, projects in build_register.items():
        yield f'## Group `{group_slug}`\n'
        for project_slug, builds in projects.items():
            yield f'### Project `{project_slug}`\n'
            builds_sorted = sorted(builds, key=lambda x: build_ids[x], reverse=True)
            for build_slug in builds_sorted:
                link = f'./{group_slug}/{project_slug}/{build_slug}.html'
                yield f'- [`{build_slug}`]({link})\n'


def create_overview(build_register, base_dir):
    """ Create overview report for all projects in a group """
    md_str = '\n'.join(list(gen_overview(build_register)))
    template_file = pl.Path(__file__).parent / 'template.html'
    html_str = tools.markdown_to_html(md_str, template_file=template_file)
    with open(base_dir / 'index.html', 'w', encoding='utf-8') as file:
        file.write(html_str)


def report(base_dir, squad_url, **kwargs):
    """ Generate report for a specific build """
    SquadApi.configure(url=squad_url)
    for key, value in (kwargs).copy().items():
        if value is None:
            kwargs.pop(key)
    for var in ('groups', 'projects', 'builds'):
        if var in kwargs:
            kwargs[var] = tools.listify(kwargs[var])

    existing_register = register.Register(base_dir / 'register.yaml')
    kwargs['register'] = existing_register

    report_builds = tools.create_nested_dict(3, final_type=list)
    for group_slug, project_slug, build_slug in group_reports(base_dir, squad_url, **kwargs):
        report_builds[group_slug][project_slug].append(build_slug)
    new_register = existing_register.merge(report_builds)
    tools.store_yaml(new_register, base_dir / 'register.yaml')

    create_overview(new_register, base_dir)
