"""" Build release reports for the CIP project """""

import argparse
import pathlib
from cip_release_reports import reports


def main():
    """ Build release reports for the CIP project. """
    parser = argparse.ArgumentParser(description='Build release reports for the CIP project')

    parser.add_argument('--base_dir', type=str,
                        default='./public',
                        help='Base directory for reports')
    parser.add_argument('--squad_url', type=str,
                        default='https://squad.ciplatform.org',
                        help='URL for SQUAD API')
    parser.add_argument('--group', type=str,
                        help='Group slug')
    parser.add_argument('--project', type=str,
                        help='Project slug')
    parser.add_argument('--build', type=str,
                        help='Build slug')
    parser.add_argument('--force', action='store_true',
                        default=False,
                        help='Force rebuild of reports')

    args = parser.parse_args()

    reports.report(
        base_dir=pathlib.Path(args.base_dir),
        squad_url=args.squad_url,
        groups=args.group,
        projects=args.project,
        builds=args.build,
        force=args.force,
    )


if __name__ == "__main__":
    main()
