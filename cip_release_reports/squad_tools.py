"""  This module contains SQUAD client related functions """

from squad_client.core.models import Squad
from squad_client.core.models import ALL
from squad_client import shortcuts


def split_environment(environment_slug, project_slug):
    """  Split environment string into defconfig definition and device type """
    if project_slug == 'iec-layer-testing':
        if not 'iec-layer-testing' in environment_slug:
            return environment_slug, None
        splitted = environment_slug.split('_')
        defconfig = splitted[0]
        if device := splitted[1]:
            return defconfig, device
    else:
        if not 'defconfig' in environment_slug:
            return environment_slug, None
        splitted = environment_slug.split('_defconfig')
        defconfig = splitted[0] + '_defconfig'
        if device := splitted[1]:
            return defconfig, device[1:]
    return defconfig, None


def acquire_build(project, build_slug='latest'):
    """ Acquire build from SQUAD """
    if build_slug == 'latest':
        return shortcuts.get_build(build_slug, project)
    return project.build(build_slug)


def project_build_versions(project):
    """ Yield all build versions for a project """
    for build in project.builds(count=ALL).values():
        yield build.version

def all_build_id_verions():
    """ Yield all build versions and id's """
    for build in Squad().builds(count=ALL).values():
        yield build.version, build.id


def group_project_slugs(group_slug):
    """ Yield all project slugs for a given group_slug """
    for project in Squad().group(group_slug).projects(count=ALL).values():
        yield project.slug


def group_slugs():
    """ Yield all group slugs """
    for group in Squad().groups(count=ALL).values():
        yield group.slug
