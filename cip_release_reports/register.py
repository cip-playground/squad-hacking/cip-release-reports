""" Register functions """

from cip_release_reports import tools


class Register(dict):
    """ Register class """
    def __init__(self, path):
        super().__init__(self.load_register(path))

    @staticmethod
    def load_register(path):
        """ Load register from file """
        try:
            return tools.load_yaml(path)
        except FileNotFoundError:
            return {}

    def load(self, path):
        """ Load register from file """
        self.update(self.load_register(path))

    def save(self, path):
        """ Save register to file """
        tools.store_yaml(self, path)

    def was_built(self, group_slug, project_slug, build_slug):
        """ Check if build has been built """
        if group_slug not in self:
            return False
        if project_slug not in self[group_slug]:
            return False
        if build_slug not in self[group_slug][project_slug]:
            return False
        return True

    def merge(self, new_register):
        """ Merge registers """
        for group_slug, projects in new_register.items():
            if group_slug not in self:
                self[group_slug] = {}
            for project_slug, builds in projects.items():
                if project_slug not in self[group_slug]:
                    self[group_slug][project_slug] = []
                for build in builds:
                    if build not in self[group_slug][project_slug]:
                        self[group_slug][project_slug].insert(0, build)
        return self
