""" SQUAD client fetcher module """

import pathlib as pl
from squad_client import utils
from squad_client.core import models
from cip_release_reports import tools
from cip_release_reports import squad_tools


class ProjectReporter:
    """ 
    To easily access project results, without the need to repeatedly pass
    project information
    """
    def __init__(self, base_url, project):
        self.base_url = base_url
        self.project = project
        self.environments = project.environments(count=models.ALL)
        self.suites = project.suites(count=models.ALL)
        self.builds = project.builds(count=models.ALL)

    @staticmethod
    def name_from_link(objects, link):
        """ Get name from link """
        return objects[utils.getid(link)]

    def ui_build_url(self, build):
        """ Return UI build URL """
        project_name = self.project.full_name
        return tools.compile_url(self.base_url, project_name, 'build', build.version)

    def ui_test_url(self, test):
        """ Return UI test URL """
        testrun = utils.getid(test.test_run)
        suite = self.name_from_link(self.suites, test.suite).slug
        build_version = self.name_from_link(self.builds, test.build).version
        test_name = test.short_name
        project_name = self.project.full_name
        return tools.compile_url(self.base_url, project_name,
                         'build', build_version,
                         'testrun', testrun,
                         'suite', suite,
                         'test', test_name,
                         'details')

    def tests(self, project_slug, build, status='fail'):
        """ Generate structured dict overview of tests with a given status """
        if status == 'incomplete':
            return self.incomplete_tests(build, project_slug)
        results = tools.create_nested_dict(4)
        for test in build.tests().values():
            if test.status != status:
                continue
            name = test.short_name
            env = self.name_from_link(self.environments, test.environment)
            defconfig, device = squad_tools.split_environment(env.slug, project_slug)
            suite = self.name_from_link(self.suites, test.suite).slug
            if not device:
                continue
            if name in results[defconfig][device][suite]:
                if test.id < results[defconfig][device][suite][name]['id']:
                    continue
            results[defconfig][device][suite][name] = {
                'id': test.id,
                'url': self.ui_test_url(test),
            }

        sorted_results = tools.create_nested_dict(4)
        for defconfig, device, suite, values in tools.nested_dict_iter(results, levels=3):
            sorted_results[defconfig][device][suite] = dict(sorted(values.items()))
        return sorted_results


    def incomplete_tests(self, build, project_slug):
        """ Generate structured dict of test suites, without any results due to
        incomplete test jobs """

        # First generate a lists of all test suites that did run
        completed_test_suites = tools.create_nested_dict(3)
        for test in build.tests().values():
            env = self.name_from_link(self.environments, test.environment)
            defconfig, device = squad_tools.split_environment(env.slug, project_slug)
            suite = self.name_from_link(self.suites, test.suite).slug
            completed_test_suites[defconfig][device][suite] = True

        # Then iterate through all testjobs, and check for incomplete test jobs
        # if a completed test suite (from another testjob) is present. If not,
        # register the test suite related to the test job as incomplete
        incomplete_test_suites = tools.create_nested_dict(4)
        for testjob in build.testjobs(count=models.ALL).values():
            if testjob.job_status != 'Incomplete': # Note the capital I!
                continue
            env_slug = testjob.environment
            defconfig, device = squad_tools.split_environment(env_slug, project_slug)
            suite = testjob.name.split('_')[-1]
            if suite not in completed_test_suites[defconfig][device]:
                incomplete_test_suites[defconfig][device][suite]['incomplete'] = {
                    'id': testjob.id,
                    'url': testjob.url,
                }
        return incomplete_test_suites


    @staticmethod
    def colored_icon(color='green', icon='✓'):
        """ Return colored icon """
        return f'<span style="color:{color}">{icon}</span>'

    def tests_table_data(self, project_slug, test_results, icon='✗', color='red'):
        """ Context manager for test table data """
        no_entries = True
        for defconfig, device, suites in tools.nested_dict_iter(test_results, levels=2):
            if project_slug == 'iec-layer-testing':
                yield f'\n<details class="h3marker"><summary><h3> Test Suite: `{defconfig}` - Target: `{device}`</h3></summary>\n\n'
            else:
                yield f'\n<details class="h3marker"><summary><h3> Defconfig: `{defconfig}` - Device: `{device}`</h3></summary>\n\n'
            header = ['Suites']
            table = ['**Tests**']
            for suite, tests in suites.items():
                header.append(f'`{suite}`')
                cell = ''
                for test, data in tests.items():
                    cell += f'<nobr>**{self.colored_icon(color, icon)}** [`{test}`]({data["url"]})</nobr><br/>'
                table.append(cell)
            yield tools.html_table(header, [table])
            yield '</details>\n\n'
        if no_entries:
            return

    @staticmethod
    def large_text(text, size=5):
        """ Return large text """
        return f'<font size="{size}">' + text + '</font>'

    def checked_large_text(self, text, size=5, color='green', icon='✓'):
        """ Return large checked text"""
        colored_icon = self.colored_icon(color, icon)
        return self.large_text(f'**{colored_icon}** {text}', size=size)

    def generate_section(self, project_slug, build, status, header_text, **kwargs):
        """ Generate tests report section """
        color = kwargs.get('color', 'red')
        icon = kwargs.get('icon', '✗')
        message = ''
        if emphasize:= kwargs.get('emphasize'):
            message = self.checked_large_text('One or more tests have **FAILED**!',
                                              color=color, icon=icon)
        tests = self.tests(project_slug, build, status=status)
        header = f'<details class="h2marker"><summary><h2>{header_text}</h2>\n</summary>\n\n'
        if content := list(self.tests_table_data(project_slug, tests, color=color, icon=icon)):
            return message + header + '\n'.join(content) + '\n\n</details>\n\n'
        if emphasize:
            return self.checked_large_text('All tests have **PASSED**!')
        return f'There are no {header_text.lower()}!\n\n</details>\n\n'

    def generate_build_section(self, build, project_slug):
        """ Generate build section """
        build_results = {}
        for test in build.tests().values():
            name = test.short_name
            if name != 'build':
                continue
            env = self.name_from_link(self.environments, test.environment)
            defconfig, _ = squad_tools.split_environment(env.slug, project_slug)
            url = self.ui_test_url(test)
            index = test.id
            if defconfig in build_results:
                if build_results[defconfig]['index'] > index:
                    continue
            build_results[defconfig] = {
                'index': index,
                'url': url,
                'status': test.status,
            }
        no_fails = True
        header = ['Defonfig', 'Status']
        table = []
        for defconfig, result in build_results.items():
            status = f'<nobr>**{self.colored_icon("green", "✓")}** [Pass]({result["url"]})</nobr>'
            if result['status'] == 'fail':
                no_fails = False
                status = f'<nobr>**{self.colored_icon("red", "✗")}** [Fail]({result["url"]})</nobr>'
            table.append([f'`{defconfig}`', status])
        if no_fails:
            yield self.checked_large_text('All builds were successful!')
        else:
            yield self.checked_large_text('One ore more builds have failed!', color='red', icon='✗')
        yield '\n<details class="h2marker"><summary><h2>Build results</h2></summary>\n\n'
        yield tools.html_table(header, table)
        yield '</details>\n\n'

    def generate(self, project_slug, build, output_dir, output_file):
        """ Generate tests report for given build """
        build_url = self.ui_build_url(build)

        incomplete_jobs = self.generate_section(project_slug, build, 'incomplete', 'Incomplete test jobs', color='firebrick', icon='⚡')

        md_str = f'# Test results of build [`{build.version}`]({build_url})\n'
        if not ('There are no' in incomplete_jobs):
            md_str += '  \n> ⚠ Due to **incomplete test jobs** some of the results are missing!\n\n'
        md_str += self.generate_section(project_slug, build, 'fail', 'Failed Tests', emphasize=True)
        md_str += self.generate_section(project_slug, build, 'xfail', 'Known issues', color='blue')
        md_str += self.generate_section(project_slug, build, 'pass', 'Passed tests', color='green', icon='✓')
        md_str += self.generate_section(project_slug, build, 'skip', 'Skipped tests', color='goldenrod', icon='⤵️')
        md_str += incomplete_jobs
        if project_slug != 'iec-layer-testing':
            md_str += f'# Build results of build [`{build.version}`]({build_url})\n'
            md_str += '\n'.join(list(self.generate_build_section(build, project_slug)))
        template_file = pl.Path(__file__).parent / 'template.html'
        html_str = tools.markdown_to_html(md_str, template_file=template_file)
        with open(output_dir / output_file, 'w', encoding='utf-8') as html_file:
            html_file.write(html_str)
