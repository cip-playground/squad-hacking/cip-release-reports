""" Tools for building release reports from SQUAD """

import collections
from string import Template
import yaml
import markdown2 as md
from cip_release_reports import register


def html_tagged(tag, content, break_char='\n', converter=None):
    """ Return content with html tags around it """
    if converter:
        content = converter(content)
    return f'<{tag}>{break_char}{content}{break_char}</{tag}>\n'


def html_table_row(row_cells, tag='td'):
    """ Returns a string of html table row """
    row = ''
    for cell in row_cells:
        row += html_tagged(tag, cell, break_char='', converter=md.markdown)
    return html_tagged('tr', row)


def html_table(header, data):
    """ Return HTML table string given a header list and table data (list of
    lists) """
    table = html_table_row(header, 'th')
    for row in data:
        table += html_table_row(row)
    return html_tagged('table', table)


def html_template(html_body_str, template_file='template.html'):
    """ Generates a full html file string for a given html body """
    with open(template_file, 'r', encoding='utf-8') as tmpl_file:
        template = Template(tmpl_file.read())
    return template.substitute(html_body=html_body_str)


def markdown_to_html(markdown_str, template_file='template.html'):
    """ Convert markdown to html """
    return html_template(md.markdown(markdown_str), template_file=template_file)


def nested_dict_iter(nested, *keytail, **opts):
    """
    Recursively iterate over a nested dict

    Eventually returns all dict keys and the values of the deepest nested dict
    """
    levels = opts.get('levels', float('inf'))
    for key, value in nested.items():
        if isinstance(value, collections.abc.Mapping) and len(keytail) < levels-1:
            yield from nested_dict_iter(value, *(list(keytail) + [key]), **opts)
        else:
            yield *keytail, key, value


def create_nested_dict(levels, final_type=dict):
    """ Create a nested dict with a given number of levels """
    if levels == 1:
        return final_type()
    return collections.defaultdict(
              lambda: create_nested_dict(levels-1, final_type=final_type)
           )


def compile_url(*parts):
    """ Compile url from parts """
    url_parts = [str(x) for x in parts]
    return '/'.join([x[:-1] if x.endswith('/') else x for x in url_parts])


def store_yaml(data, file_path):
    """ Store data in yaml file """
    yaml.add_representer(collections.defaultdict,
        yaml.representer.Representer.represent_dict)
    yaml.add_representer(register.Register,
        yaml.representer.Representer.represent_dict)
    with open(file_path, 'w', encoding='utf-8') as file:
        yaml.dump(data, file, sort_keys=False, default_flow_style=False)


def load_yaml(file_path):
    """ Load data from yaml file """
    with open(file_path, 'r', encoding='utf-8') as file:
        return yaml.safe_load(file)


def listify(variable):
    """ Shorthand to assure variable is a list """
    if isinstance(variable, (list, tuple)):
        return variable
    return [variable]
