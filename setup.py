""" Setup script for the cip-release-reports package """

from setuptools import setup


with open('requirements.txt', mode='r', encoding='utf-8') as file:
    REQUIREMENTS = file.readlines()


with open('README.md', mode='r', encoding='utf-8') as file:
    README = file.read()


setup(
    name='cip-release-reports',
    version='1.0',
    author='Sietze van Buuren',
    author_email='sietze.vanbuuren@de.bosch.com',
    python_requires=">=3.9",
    packages=['cip_release_reports'],
    package_dir={'cip_release_reports': 'cip_release_reports'},
    package_data={'cip_release_reports': ['*.html']},
    entry_points={
        'console_scripts': [
            'cip-release-reports=cip_release_reports.build_release_reports:main'
        ]
    },
    url='https://gitlab.com/cip-playground/squad-hacking/cip-release-reports',
    description='Generates release reports for CIP Linux Kernel builds',
    long_description=README,
    install_requires=REQUIREMENTS,
)
